package com.wen.algorithm.t1380矩阵中的幸运数;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/15 08:36
 **/
public class Solution {

    /**
     * 给你一个 m * n 的矩阵，矩阵中的数字 各不相同 。请你按 任意 顺序返回矩阵中的所有幸运数。
     *
     * 幸运数是指矩阵中满足同时下列两个条件的元素：
     *
     * 在同一行的所有元素中最小
     * 在同一列的所有元素中最大
     *  
     * 示例 1：
     *
     * 输入：matrix = [[3,7,8],[9,11,13],[15,16,17]]
     * 输出：[15]
     * 解释：15 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。
     * 示例 2：
     *
     * 输入：matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
     * 输出：[12]
     * 解释：12 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。
     * 示例 3：
     *
     * 输入：matrix = [[7,8],[1,2]]
     * 输出：[7]
     *
     */
    public static List<Integer> luckyNumbers(int[][] matrix) {
        int a = matrix.length;
        int b = matrix[0].length;
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                boolean isMin = true, isMax = true;
                for (int k = 0; k < b; k++) {
                    if (matrix[i][k] < matrix[i][j]) {
                        isMin = false;
                        break;
                    }
                }

                if (!isMin) {
                    continue;
                }

                for (int k = 0; k < a; k++) {
                    if (matrix[k][j] > matrix[i][j]) {
                        isMax = false;
                        break;
                    }
                }

                if (isMax) {
                    result.add(matrix[i][j]);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] matrix = {{1,10,4,2},{9,3,8,7},{15,16,17,12}};
        System.out.println(luckyNumbers(matrix));
    }
}
