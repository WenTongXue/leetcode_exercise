package com.wen.algorithm.t1791找出星型图的中心节点;

import java.util.ArrayList;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/18 08:47
 **/
public class Solution {
    /**
     * 有一个无向的 星型 图，由 n 个编号从 1 到 n 的节点组成。星型图有一个 中心 节点，并且恰有 n - 1 条边将中心节点与其他每个节点连接起来。
     *
     * 给你一个二维整数数组 edges ，其中 edges[i] = [ui, vi] 表示在节点 ui 和 vi 之间存在一条边。请你找出并返回 edges 所表示星型图的中心节点。
     *
     *
     * 示例 1：
     *      4
     *      |
     *      2
     *     / \
     *    1   3
     *
     * 输入：edges = [[1,2],[2,3],[4,2]]
     * 输出：2
     * 解释：如上图所示，节点 2 与其他每个节点都相连，所以节点 2 是中心节点。
     * 示例 2：
     *
     * 输入：edges = [[1,2],[5,1],[1,3],[1,4]]
     * 输出：1
     *
     */
    public static int findCenter1(int[][] edges) {
        return edges[0][0] == edges[1][0] || edges[0][0] == edges[1][1] ? edges[0][0] : edges[0][1];
    }

    public static int findCenter2(int[][] edges) {
        int n = edges.length + 1;
        int[] degrees = new int[n + 1];
        for (int[] edge : edges) {
            degrees[edge[0]]++;
            degrees[edge[1]]++;
        }
        for (int i = 1; i <= n; i++) {
            if (degrees[i] == n - 1) {
                return i;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        int[][] edges = {{1,2}, {5,1}, {1,3}, {1,4}};
        findCenter2(edges);
    }

}
