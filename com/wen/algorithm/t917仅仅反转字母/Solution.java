package com.wen.algorithm.t917仅仅反转字母;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/23 09:05
 **/
public class Solution {

    /**
     * 给你一个字符串 s ，根据下述规则反转字符串：
     *
     * 所有非英文字母保留在原有位置。
     * 所有英文字母（小写或大写）位置反转。
     * 返回反转后的 s 。
     *
     *  
     *
     * 示例 1：
     *
     * 输入：s = "ab-cd"
     * 输出："dc-ba"
     * 示例 2：
     *
     * 输入：s = "a-bC-dEf-ghIj"
     * 输出："j-Ih-gfE-dCba"
     * 示例 3：
     *
     * 输入：s = "Test1ng-Leet=code-Q!"
     * 输出："Qedo1ct-eeLg=ntse-T!"
     *
     */
    public static String reverseOnlyLetters1(String s) {

        int length = s.length();
        int a = 0;
        HashMap<Integer, Character> map = new HashMap<>();
        ArrayList<Character> list = new ArrayList<>();
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < length; i++) {
            if (!Character.isLetter(s.charAt(i))) {
                map.put(i, s.charAt(i));
            }
            if (Character.isLetter(s.charAt(length - 1 - i))) {
                list.add(s.charAt(length - 1 - i));
            }
        }
        for (int k = 0; k < length; k++) {
            if (map.containsKey(k)) {
                str.append(map.get(k));
            } else {
                str.append(list.get(a));
                a++;
            }
        }

        return str.toString();
    }

    public static String reverseOnlyLetters2(String s) {
        int n = s.length();
        char[] arr = s.toCharArray();
        int left = 0, right = n - 1;
        while (true) {
            while (left < right && !Character.isLetter(s.charAt(left))) { // 判断左边是否扫描到字母
                left++;
            }
            while (right > left && !Character.isLetter(s.charAt(right))) { // 判断右边是否扫描到字母
                right--;
            }
            if (left >= right) {
                break;
            }
            swap(arr, left, right);
            left++;
            right--;
        }
        return new String(arr);
    }

    public static void swap(char[] arr, int left, int right) {
        char temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
    }


    public static void main(String[] args) {
        String s = "Test1ng-Leet=code-Q!";
        System.out.println(reverseOnlyLetters2(s));
    }
}
