package com.wen.algorithm.t537复数乘法;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/25 08:50
 **/
public class Solution {

    /**
     * 复数 可以用字符串表示，遵循 "实部+虚部i" 的形式，并满足下述条件：
     *
     * 实部 是一个整数，取值范围是 [-100, 100]
     * 虚部 也是一个整数，取值范围是 [-100, 100]
     * i^2 == -1
     * 给你两个字符串表示的复数 num1 和 num2 ，请你遵循复数表示形式，返回表示它们乘积的字符串。
     *
     * 示例 1：
     *
     * 输入：num1 = "1+1i", num2 = "1+1i"
     * 输出："0+2i"
     * 解释：(1 + i) * (1 + i) = 1 + i2 + 2 * i = 2i ，你需要将它转换为 0+2i 的形式。
     * 示例 2：
     *
     * 输入：num1 = "1+-1i", num2 = "1+-1i"
     * 输出："0+-2i"
     * 解释：(1 - i) * (1 - i) = 1 + i2 - 2 * i = -2i ，你需要将它转换为 0+-2i 的形式。
     *
     */
    public static String complexNumberMultiply(String num1, String num2) {
        String[] nums1 = num1.split("\\+|i");
        String[] nums2 = num2.split("\\+|i");
        int a = Integer.parseInt(nums1[0]);
        int b = Integer.parseInt(nums1[1]);
        int c = Integer.parseInt(nums2[0]);
        int d = Integer.parseInt(nums2[1]);
        int first = (a * c) - (b * d);
        int last = (a * d) + (b * c);
        return String.valueOf(first) + "+" + String.valueOf(last) + "i";
    }

    public static void main(String[] args) {
        String num1 = "1+-1i";
        String num2 = "0+0i";
        System.out.println(complexNumberMultiply(num1, num2));
    }
}
