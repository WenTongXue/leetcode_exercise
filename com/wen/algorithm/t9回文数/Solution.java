package com.wen.algorithm.t9回文数;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/01/25 11:59
 **/
public class Solution {

    /**
     * 给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。
     *
     * 回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。
     * 示例 1：
     *
     * 输入：x = 121
     * 输出：true
     * 示例 2：
     *
     * 输入：x = -121
     * 输出：false
     * 解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
     * 示例 3：
     *
     * 输入：x = 10
     * 输出：false
     * 解释：从右向左读, 为 01 。因此它不是一个回文数。
     * 示例 4：
     *
     * 输入：x = -101
     * 输出：false
     *
     */


    /**
     * 方法一
     * @param x
     * @return
     */
    public static boolean isPalindrome1(int x) {
        String value = String.valueOf(x);
        StringBuffer str = new StringBuffer();
        for (int i = value.length() - 1; i >= 0; i --) {
            char c = value.charAt(i);
            str.append(c);
        }

        if (str.equals(Integer.valueOf(x))) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isPalindrome2(int x) {
        String s = String.valueOf(x);
        String reverse = new StringBuffer(s).reverse().toString();
        if (s.equals(reverse)){
            return true;
        }
        return false;
    }

    /**
     * 方法二
     * @param x
     * @return
     */
    public static boolean isPalindrome3(int x) {
        if (x < 0) {
            return false;
        }

        int res = 0;
        int cup = x;
        while (cup != 0) {
            res = res * 10 + cup % 10;
            cup = cup / 10;
        }
        return res == x;
    }


    public static void main(String[] args) {
        System.out.println(isPalindrome3(121));
    }
}
