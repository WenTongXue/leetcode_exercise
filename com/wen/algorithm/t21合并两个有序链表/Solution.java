package com.wen.algorithm.t21合并两个有序链表;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/16 08:50
 **/
public class Solution {

    /**
     * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
     * 示例 1：
     * 1 --> 2 --> 4
     * 1 --> 3 --> 4
     * ----------------
     * 1 --> 1 --> 2 --> 3 --> 4 --> 4
     *
     * 输入：l1 = [1,2,4], l2 = [1,3,4]
     * 输出：[1,1,2,3,4,4]
     * 示例 2：
     *
     * 输入：l1 = [], l2 = []
     * 输出：[]
     * 示例 3：
     *
     * 输入：l1 = [], l2 = [0]
     * 输出：[0]
     *
     */
    public static ListNode mergeTwoLists(ListNode lists1, ListNode lists2) {
        if (lists1 == null) {
            return lists2;
        }
        if (lists2 == null) {
            return lists1;
        }
        ListNode prehead = new ListNode(-1);

        ListNode prev = prehead;
        while (lists1 != null && lists2 != null) {
            if (lists1.val <= lists2.val) {
                prev.next = lists1;
                lists1 = lists1.next;
            } else {
                prev.next = lists2;
                lists2 = lists2.next;
            }
            prev = prev.next;
        }

        // 合并后 l1 和 l2 最多只有一个还未被合并完，我们直接将链表末尾指向未合并完的链表即可
        prev.next = lists1 == null ? lists2 : lists1;

        return prehead.next;
    }

    public static void main(String[] args) {
        ListNode lists1 = new ListNode(1);
        ListNode lists12 = new ListNode(2);
        ListNode lists13 = new ListNode(4);
        lists1.next = lists12;
        lists12.next = lists13;
        ListNode lists2 = new ListNode(1);
        ListNode lists22 = new ListNode(3);
        ListNode lists23 = new ListNode(4);
        lists2.next = lists22;
        lists22.next = lists23;
        mergeTwoLists(lists1, lists2);
    }


    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) {
            this.val = val;
        }
        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
