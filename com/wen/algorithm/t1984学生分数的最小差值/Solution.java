package com.wen.algorithm.t1984学生分数的最小差值;

import java.util.Arrays;

/**
 * @Author: gwen
 * @Description:
 * @CreateDate: 2022/02/11 08:46
 **/
public class Solution {
    /**
     * 给你一个 下标从 0 开始 的整数数组 nums ，其中 nums[i] 表示第 i 名学生的分数。另给你一个整数 k 。
     *
     * 从数组中选出任意 k 名学生的分数，使这 k 个分数间 最高分 和 最低分 的 差值 达到 最小化 。
     *
     * 返回可能的 最小差值 。
     *
     * 示例 1：
     *
     * 输入：nums = [90], k = 1
     * 输出：0
     * 解释：选出 1 名学生的分数，仅有 1 种方法：
     * - [90] 最高分和最低分之间的差值是 90 - 90 = 0
     * 可能的最小差值是 0
     * 示例 2：
     *
     * 输入：nums = [9,4,1,7], k = 2
     * 输出：2
     * 解释：选出 2 名学生的分数，有 6 种方法：
     * - [9,4,1,7] 最高分和最低分之间的差值是 9 - 4 = 5
     * - [9,4,1,7] 最高分和最低分之间的差值是 9 - 1 = 8
     * - [9,4,1,7] 最高分和最低分之间的差值是 9 - 7 = 2
     * - [9,4,1,7] 最高分和最低分之间的差值是 4 - 1 = 3
     * - [9,4,1,7] 最高分和最低分之间的差值是 7 - 4 = 3
     * - [9,4,1,7] 最高分和最低分之间的差值是 7 - 1 = 6
     * 可能的最小差值是 2
     */
    public static int minimumDifference1(int[] nums, int k) {
        int length = nums.length;
        int temp;
        for (int i = 0; i < length - 1; i++) {
            for (int j = 0; j < length - 1 - i; j++) {
                if (nums[j] > nums[j + 1]) {
                    temp = nums[j + 1];
                    nums[j + 1] = nums[j];
                    nums[j] = temp;
                }
            }
        }
        return nums[length - 1] - nums[length - k];
    }

    public static int minimumDifference2(int[] nums, int k) {
        int n = nums.length;
        Arrays.sort(nums);
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i + k - 1 < n; ++i) {
            ans = Math.min(ans, nums[i + k - 1] - nums[i]);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] nums = {9, 4, 1, 7};
        int k = 3;
        System.out.println(minimumDifference2(nums, k));
    }
}
